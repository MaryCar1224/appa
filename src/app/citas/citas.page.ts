//import { CitasPageRoutingModule } from './citas-routing.module';
import { Component, OnInit, ViewChild} from '@angular/core';
import {IonContent} from '@ionic/angular';
import {CitasService}  from './../servicios/citas.service';
//import { setOptions, localeEs } from '@mobiscroll/angular';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Citasmodels } from './../models/citasmodels';
import { environment } from './../../environments/environment';
//import { Observable } from 'rxjs'
import { ClienteService } from './../servicios/cliente.service';

import * as moment from 'moment';
import "moment/locale/es";
import { TokenService } from '../servicios/token.service';
moment.locale('es');

const hoy = moment();

@Component({
  selector: 'app-citas',
  templateUrl: './citas.page.html',
  styleUrls: ['./citas.page.scss'],
})
export class CitasPage implements OnInit {

  @ViewChild(IonContent, {static: true}) content: IonContent;
  //isEnd = false;
  semana:any=[
    "Lunes",
    "Martes",
    "Miercoles",
    "Jueves",
    "Viernes", 
    "Sabado",
    "Domingo"
  ];
  
  citas:Citasmodels[]=[];
  monthSelect: any[];
  dateSelect: any;


  constructor(private CS:CitasService, private httpClient:HttpClient, private TS:TokenService,
    private ClienteService:ClienteService) { }

  ngOnInit():void{
    //this.cargar();
    this.getDaysFromDate(9,2021);

  }

//Muestra el calendario 
  getDaysFromDate(month,year){ 
    const startDate = moment(`${year}/${month}/01`) 
    const endDate = startDate.clone().endOf('month')
    this.dateSelect=startDate;

    const diffDays = endDate.diff(startDate, 'days', true)
    const numberDays = Math.round(diffDays);

    const arrayDays = Object.keys([ ...Array(numberDays)]).map((a:any) =>{
      a= parseInt(a) + 1;
      const dayObject = moment(`${year}-${month}-${a}`);
      return{
        name: dayObject.format("LLLL"),
        value:a,
        indexWeek: dayObject.isoWeekday()
      };
    });

    this.monthSelect = arrayDays;
    //console.log(arrayDays);
  }

//Metodo para cambiar la vista del mes
  changeMonth(flag){
    if(flag < 0){
      const prevDate = this.dateSelect.clone().subtract(1, "month");
      this.getDaysFromDate(prevDate.format("MM"), prevDate.format("YYYY"));
    }else{
      const nextDate = this.dateSelect.clone().add(1, "month");
      this.getDaysFromDate(nextDate.format("MM"), nextDate.format("YYYY"));
    }
  }

  //Metodo donde se mandan los datos del servicio citas
  clickDay(day){
    const monthYear = this.dateSelect.format('YYYY-MM')
      //console.log('monthYear', monthYear);
      //console.log('day', day.value);
    const parse = `${monthYear}-${day.value}`
    console.log(parse); //muestra en consola la fecha en el formato YYYY-MM-DD
    //const objectDate = moment(parse)
    //console.log(objectDate);

    const params = new HttpParams()
    .set('usuario', this.TS.getUserName())
    .set('club', 'Club Alpha 2')
    .set('dia', parse );

      const fullURL = `${environment.global+'citas/obtenerClases'}?${params.toString()}`;

      return this.httpClient.get<Citasmodels[]>(fullURL).subscribe((data:any) =>{
        console.log(data) 
        this.citas= data;
      }, 
      err=>{
        console.log(err)
      })
  }


  goTop() {
    this.content.scrollToTop(2000);
}

/*cargar(){

  this.CS.obtClases().subscribe(
    (data:any)=>{
      console.log(data) 
      this.citas=data;
    }, 
    err =>{
     console.log(err);
    }

  )

}*/

}
