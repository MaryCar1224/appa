import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NoticiasWebService {

  constructor(private http:HttpClient) { }
 
   noti(){
      const headers = new HttpHeaders({
        'Token': environment.Token
        });
      return this.http.get(environment.directionNoticias, { headers})
    }

}