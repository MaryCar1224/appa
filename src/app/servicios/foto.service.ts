import { Foto } from './../models/foto';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
}) 
export class FotoService {

  constructor(private httpClient:HttpClient) { }

   //Este metodo sirve para mostrar todas las fotos, las devuelve en una lista
  public obtFotos():Observable<Foto[]>{  
    return this.httpClient.get<Foto[]>(environment.global + 'obtenerFoto')
  }

  //Este metodo sirve para mostrar la foto del cliente mediante el id
  public foto(FotoId:number){
    return this.httpClient.get(environment.global + `obtenerFoto/${FotoId}`);  //obtenerFoto/{fotoId} se concateno
  }  
}
