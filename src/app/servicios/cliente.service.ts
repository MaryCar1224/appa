import { environment } from './../../environments/environment';
import { TokenService } from './token.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cliente } from './../models/cliente';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
}) 
export class ClienteService { 

  constructor(private httpClient:HttpClient, private TokenService:TokenService) { }

    //Este metodo sirve para mostrar un cliente, no necesita concatenar el id del cliente, aunque no lo pida  otra opcion :`obtenerCliente/${id}`
    public obtCliente(id:number):Observable<Cliente[]>{
      return this.httpClient.get<Cliente[]>(environment.global +'alpha/obtenerCliente/' + this.TokenService.getUserName())//'obtenerCliente' + this.TokenService.getUserName() //+ this.TokenService.getUserName                                   
    }
}
