import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {ServiciosMovimientos} from  './../models/servicios-movimientos';

@Injectable({
  providedIn: 'root'
})
export class MovimientosService {

  constructor(private http:HttpClient) { }

   public movimientos(id:number):Observable<ServiciosMovimientos[]>{ 
    return this.http.get<ServiciosMovimientos[]>(environment.global + `alpha/obtenerMovimientos/${id}`);
  }
}
