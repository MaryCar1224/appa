import { Citasmodels } from './../models/citasmodels';
import { environment } from './../../environments/environment';
import { TokenService } from './token.service';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { Cliente } from './../models/cliente';
import { Observable } from 'rxjs';
//import { RequestOptions } from 'http';


@Injectable({
  providedIn: 'root'
}) 
export class CitasService {

  constructor(private httpClient:HttpClient, private TS:TokenService) { }

      //Obtiene una lista de las clases del dia 
      public obtClases():Observable<Citasmodels[]>{

        const params = new HttpParams()
	      .set('usuario', this.TS.getUserName())
	      .set('club', 'Club Alpha 2')
        .set('dia', '2021-10-21');

        const fullURL = `${environment.global+'citas/obtenerClases'}?${params.toString()}`;
        console.log( fullURL );
 
        return this.httpClient.get<Citasmodels[]>(fullURL)
        
      }

}
