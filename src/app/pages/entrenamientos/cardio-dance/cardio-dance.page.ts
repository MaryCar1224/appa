import { VideosService } from './../videos.service';
import { AnimationListModel } from './../../../components/custom-animation-list1/animation-list.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cardio-dance',
  templateUrl: './cardio-dance.page.html',
  styleUrls: ['./cardio-dance.page.scss'],
})
export class CardioDancePage implements OnInit {
  list: Array<AnimationListModel>;
  animationClassName = 'fade-in-bl';

  constructor(private service: VideosService) {
      this.list = this.service.getList();
  }

  ngOnInit() {
  }
}
