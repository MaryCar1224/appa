import {Injectable} from '@angular/core';
import {AnimationListModel} from '../../components/custom-animation-list1/animation-list.model';

@Injectable({providedIn: 'root'})
export class VideosService {
    getList(): Array<AnimationListModel> {
        return [
            {name: 'Principiantes',content:'Parte uno',  likes: 90,comments:50,vistos:34,visto:true,url:'462195097',fecha:'04/07/2021'},
            {name: 'Principiantes',content:'Parte dos',  likes: 56,comments:54,vistos:65,visto:false,url:'461644402',fecha:'06/07/2021'},
            {name: 'Principiantes',content:'Parte tres',  likes: 56,comments:34,vistos:8,visto:false,url:'462191620',fecha:'08/07/2021'},
            {name: 'Intermedio',content:'Parte uno',  likes: 64,comments:23,vistos:76,visto:true,url:'467423376',fecha:'21/07/2021'},
            {name: 'Intermedio',content:'Parte dos',  likes: 34,comments:76,vistos:2,visto:true,url:'487053824',fecha:'30/07/2021'},
            {name: 'Intermedio',content:'Parte tres',  likes: 7,comments:54,vistos:23,visto:true,url:'497004046',fecha:'15/07/2021'},
            {name: 'Avanzado',content:'Parte uno',  likes: 453,comments:123,vistos:76,visto:true,url:'461645149',fecha:'18/07/2021'},
            {name: 'Avanzado',content:'Parte dps',  likes: 54,comments:645,vistos:34,visto:true,url:'486086724',fecha:'17/07/2021'},
            {name: 'Avanzado',content:'Parte tres',  likes: 54,comments:35,vistos:69,visto:true,url:'467522628',fecha:'08/07/2021'},
        ];
    }
}
