import { environment } from './../../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'; 
import { NoticiasWebService } from './../../servicios/noticiasWeb.service';
//import { ServiciosNoticias } from './../../models/servicios-noticias'; //se importa para el uso de este servicio
import { Component, OnInit, ViewChild } from '@angular/core';
import { NoticiasService } from './noticias.service';
import {IonContent} from '@ionic/angular';
//import * as sanitizeHtml from 'sanitize-html';


@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.page.html',
  styleUrls: ['./noticias.page.scss'],
  
})
export class NoticiasPage implements OnInit { 

  @ViewChild(IonContent, {static: true}) content: IonContent;
  list: Array<any>; 
  noticias:any; //se almacenan los datos del web services
  //data: SafeResourceUrl;
 // directionNoticias: 'http://d09852f23226.ngrok.io/clubalpha/api/v1/category/list/19/content';
 //htmlSnippet = 'Template <script>alert("0wned")</script> <b>Syntax</b>';
  
  constructor(private service:NoticiasService, public NoticiasWebService:NoticiasWebService, 
    /*private sanitizer:DomSanitizer*/) {
    this.list = this.service.getList();
   }

  ngOnInit() {
  this.getPosts();
  }

  getPosts() { //llamamos a la funcion noti de nuestro servicio NoticiasWebServicio.
    this.NoticiasWebService.noti().subscribe(
      data=> 
      {
        console.log(data)  //muestra los datos en consola 
        this.noticias = data;
        
        //this.data =
        //this.sanitizer.bypassSecurityTrustResourceUrl(environment.directionNoticias);
      
        //ejemplo de sanitizeHtml
        //console.log(sanitizeHtml("<p>('hello world')</p>")); 
        //dangerousVideoUrl = ("<p>('hello world')</p>"));
        
        //this.noticias =this.sanitizer.bypassSecurityTrustResourceUrl(this.directionNoticias);
        //console.log(this.noticias);

      })
    /*.then(data => {    //para guardar en una variable los datos de la consulta json 
      this.arrayPosts = data;
    });*/

  }

// método para el scroll infinito
  //loadData(event) {
   // console.log("Cargando...");
    //setTimeout(() => {
      //console.log('Done');
      //event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
     // if (data.length == 1000) {
       // event.target.disabled = true;
      //}
    //}//, 500);

    loadData(event) {
      setTimeout(() => {
          this.noticias = [this.noticias];
          this.list = this.noticias;
          event.target.complete();
          if (this.list.length === 5) {
              event.target.disabled = true;
          }
      }, 2000);
  }

    onSearch(event) {
      const key = event.target.value.toLowerCase();
      this.list = this.noticias.filter(p => p.name.toLowerCase().includes(key));
  }

  goTop() {
    this.content.scrollToTop(2000);
}
    
}
