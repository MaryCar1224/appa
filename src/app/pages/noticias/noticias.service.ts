import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class NoticiasService {
    getList(): Array<any> {
        return [
            {
                name: 'Clubs Alpha',
                date: '25-07-2021',
                avatar: 'assets/images/avatar/1.jpg',
                title: 'Nuevas modalidades de entrenamiento',
                img: 'assets/images/list/15.jpg',
                content:'Descubre las nuevas modalidades de entrenamiento recientemente agregadas',
                icon:'heart'
            },
            {
                name: 'Mónica Torroella',
                date: '26-07-2021',
                avatar: 'assets/images/avatar/2.jpg',
                title: 'Nuevo video de Spining disponible',
                img: 'assets/images/list/16.jpg',
                content:'Descubre las nuevas modalidades de entrenamiento recientemente agregadas',
                icon:'heart-outline'
            },
            {
                name: 'Mario Rivera',
                date: '22-07-2021',
                avatar: 'assets/images/avatar/3.jpg',
                title: 'Las artes marciales y el entrenamiento',
                img: 'assets/images/list/17.jpg',
                content:'Descubre las nuevas modalidades de entrenamiento recientemente agregadas',
                icon:'heart-outline'
            },
            {
                name: 'María José Murieda',
                date: '23-07-2021',
                avatar: 'assets/images/avatar/4.jpg',
                title: 'Rutnas de entrenamiento nuevas',
                img: 'assets/images/list/18.jpg',
                content:'Descubre las nuevas modalidades de entrenamiento recientemente agregadas',
                icon:'heart'
            },
            {
                name: 'Elizabeth Ríos',
                date: '24-07-2021',
                avatar: 'assets/images/avatar/5.jpg',
                title: 'Los retos del mes',
                img: 'assets/images/list/19.jpg',
                content:'Descubre las nuevas modalidades de entrenamiento recientemente agregadas',
                icon:'heart-outline'
            },

        ];
    }
}
