import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'ObjToArray',
})

export class ObjToArrayPipe implements PipeTransform{
    transform(objects:any =[]):any{
        return Object.values(objects);
    }
}