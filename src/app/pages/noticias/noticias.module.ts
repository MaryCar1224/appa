import { ObjToArrayPipe } from './objToArray.pipe';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
//import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { IonicModule } from '@ionic/angular';

import { NoticiasPageRoutingModule } from './noticias-routing.module';

import { NoticiasPage } from './noticias.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoticiasPageRoutingModule,
    ComponentsModule,
    HttpClientModule
    //InfiniteScrollModule
  ],

  declarations: [NoticiasPage,ObjToArrayPipe],  //se habia agregado NoticiasWebServices para quitar error
  //providers:[HttpHeaders] //se habia agregado por error de los providers en headers
          //daba otro error y se quito 
})
export class NoticiasPageModule {} 