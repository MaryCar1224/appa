import { ComprasService } from './compras.service';
import { Component, OnInit } from '@angular/core';
import {timer} from 'rxjs';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.page.html',
  styleUrls: ['./compras.page.scss'],
})
export class ComprasPage implements OnInit {
  list: Array<any>;
  isLoading = true;

  constructor(private service: ComprasService) {
    this.list = this.service.getList();
  }

  ngOnInit() {
    timer(2000).subscribe(r => {
      this.isLoading = !this.isLoading;
    });
  }
}
