import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class Home1Service {

    getBannerList(): Array<string> {
        return [
            'assets/images/list/1.jpg',
            'assets/images/list/2.jpg',
            'assets/images/list/3.jpg'
        ];
    }

    getList(): Array<any> {
        return [
            {icon: 'happy-outline', title: 'Asesoría personalizada', content: 'Obtén los mejores resultados con el servicio de asesor deportivo.'},
            {icon: 'speedometer-outline', title: 'Niveles de entrenamiento', content: 'Para lograr tus objetivos hemos creado tres niveles de entrenamiento.'},
            {
                icon: 'calendar-outline',
                title: 'Retos distintos cada mes',
                content: 'Te daremos la dosis diaria necesaria para cumplir el reto.'
            },
            {icon: 'people-outline', title: 'Invitados especiales ', content: 'Cada mes una nueva serie de invitados especiales.'}
        ];
    }

    getFList(): Array<any> {
        return [
            {
                title: 'Body Combat',
                content: 'Inspirado en las artes marciales donde se realizan golpes, puñetazos, patadas con coreografías musicales.'
            },
            {
                title: 'Indoor ciclyng',
                content: 'Es un ejercicio que consiste en rodar una bicicleta fija, donde se trabaja principalmente el tren inferior.'
            },
            {
                title: 'Body fitness',
                content: 'Es un entrenamiento donde se realiza un determinado número de circuitos con diferentes coreografías.'
            },
            {
                title: 'Fitness attack',
                content: 'Es un entrenamiento cardiovascular donde los participantes realizan puñetazos, patadas y golpes de combate.'
            },
        ];
    }

    getTList(): Array<any> {
        return [
            {name: 'Mónica Torroella', img: 'assets/images/avatar/2.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'},
            {name: 'Mario Rivera', img: 'assets/images/avatar/3.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'},
            {name: 'María José Murieda', img: 'assets/images/avatar/4.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'},
            {name: 'Elizabeth Ríos', img: 'assets/images/avatar/5.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'}
        ];
    }

    getNewList(): Array<any> {
        return [
            {title: 'Nuevo video de Spinning', img: 'assets/images/list/21.jpg',content: 'Ya tenemos listo un nuevo video para la disciplina de Spinning.'},
            {title: 'Reto "Piernas de acero"', img: 'assets/images/list/22.jpg',content: 'El nuevo reto "Piernas de acero" está disponible ahora.'},
            {title: 'Nuevos invitados', img: 'assets/images/list/23.jpg',content: 'Ahora hay nuevos invitados para el mes de agosto.'},
            {title: 'Artes marciales y el ejercicio', img: 'assets/images/list/24.jpg',content: 'Descubre las rutinas de ejercicio basadas en artes marciales'},
        ];
    }

}
