// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-custom-side-right',
//   templateUrl: './custom-side-right.component.html',
//   styleUrls: ['./custom-side-right.component.scss'],
// })
// export class CustomSideRightComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {}

// }

import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-custom-side-right',
    templateUrl: './custom-side-right.component.html',
    styleUrls: ['./custom-side-right.component.scss'],
})
export class CustomSideRightComponent {
    selectType = 'SOCIAL';
    chatList = [
        {name: 'Clubs Alpha', status: 'Hoy', content: 'Nuevas membresías disponibles', img: 'assets/images/avatar/1.jpg'},
        {name: 'Mónica Torroella', status: 'Hace 1 día', content: 'Acabo de subir un video para las clases de Spinning', img: 'assets/images/avatar/2.jpg'},
        {name: 'Mario Rivera', status: 'Hce un día', content: 'Publiqué un nuevo reto en llamado "Pierna de acero"', img: 'assets/images/avatar/3.jpg'},
        {name: 'María José Murieda', status: 'Hace 2 días', content: 'No olvides revisar las nuevas rutinas de Pilates', img: 'assets/images/avatar/4.jpg'},
        {name: 'Elizabeth Ríos', status: 'Hace 3 días', content: 'Realiza la nueva rutina de alimentación', img: 'assets/images/avatar/5.jpg'},
        {name: 'Daniela Posadas', status: 'Hace 1 semana', content: 'Aprende artes marciales con los videos que subimos recientemente', img: 'assets/images/avatar/6.jpg'}
    ];
    sList = [
        {
            name: 'email',
            icon: 'mail',
            content: ' Contactanos por email a contacto@trainingh.mx.',
        },
        {
            name: 'Twitter',
            icon: 'logo-twitter',
            content: ' Mira nuestros últimos comentarios en Twitter.',
        },
        {
            name: 'Facebook',
            icon: 'logo-facebook',
            content: ' Enterate de los últimos eventos en Facebook.',
        },
        {
            name: 'Instagram',
            icon: 'logo-instagram',
            content: ' Puedes ver todas las fotos de nuestro equipo en Instagram.',
        },
        {
            name: 'YouTube',
            icon: 'logo-youtube',
            content: ' Para ver algunos tutoriales y videos sobre nosotros.',
        }
    ];

    constructor() {
    }
}
