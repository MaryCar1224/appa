export class AnimationListModel {
    public name: string;
    public img?: string;
    public avatar?: string;
    public content?: string;
    public price?: number;
    public url?: string;
    public likes?: number;
    public comments?: number;
    public vistos?: number;
    public visto?: boolean;
    public fecha?: string;
}
