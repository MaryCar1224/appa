import { ClienteService } from './../../servicios/cliente.service';
import { RouterModule, ActivatedRoute, Routes } from '@angular/router';
import { TokenService } from './../../servicios/token.service';
import { AuthService } from './../../servicios/auth.service';
import { AlertController, NavController } from '@ionic/angular';
import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';

@Component({ 
    selector: 'custom-profile2',
    templateUrl: './custom-profile2.component.html',
    styleUrls: ['./custom-profile2.component.scss'],
})

export class CustomProfile2Component {
    @Input() user: any; 
    @Input() list: any;
    //@Input() cliente:any; //esto es prueba

    nombreUsuario='';
    password='';
    isLogged=false;
    //cliente:Cliente; //se concatenaba el modelo cliente para que pudiera obtener los datos de cliente 
    cliente:any;  


    constructor(public alertController:AlertController, public AuthService:AuthService,
        private navCtrl:NavController, public TokenService:TokenService
        , private RouterModule:RouterModule,private ClienteService:ClienteService, 
        private ActivatedRoute:ActivatedRoute, private Router:Router
        ) {}

    async mostrarEC(){
        const alert = await this.alertController.create(
            {
                header:"Estado de cuenta", 
                subHeader:"Pago",
                message: this.TokenService.getUserName(),// this.TokenService.getUserName(),//this.cliente.nombrecompleto,//, //this.Cliente.nombrecompleto, aqui va el valor del estado de cuenta
                buttons: ['OK']
            } 
        );
        await alert.present(); 
        let result = await alert.onDidDismiss();
        console.log(result);
    }

    vaciar() {
        this.nombreUsuario = '';
        this.password = '';
      }
 
    logOut(): void{
        this.TokenService.logOut();
        this.isLogged = false;
        this.vaciar();
        this.navCtrl.navigateRoot('/menu/login')
    }

    EstadodeCuenta():void{
        this.Router.navigate(['/movimientos/' + this.TokenService.getUserName()])//+ this.TokenService.getUserName()])
    }

    MisDatos():void{
        this.Router.navigate(['/misdatos/' + this.TokenService.getUserName()])//+ this.TokenService.getUserName()])
    }

    ngOnInit() {
        this.cargarCliente();
      }

    cargarCliente(){
        const id = this.ActivatedRoute.snapshot.params.id; //este es para obtener el id de la url
        //console.log(id);
        //toma una variable en este caso toma la del idcliente que se encuentra en la url
        this.ClienteService.obtCliente(id).subscribe(   //aqui no creo que sea necesario jalar el id
          (data)=>{
           console.log(data)  //muestra los datos en consola 
            this.cliente=data;
            //localStorage.getItem(this.cliente.club.nombre);
          },
          err =>{
          }
        )
    }

    ObtenerCitas():void{
        this.Router.navigate(['/citas'])//+ this.TokenService.getUserName()])
    }
  
}