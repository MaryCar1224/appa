import {Component, OnInit} from '@angular/core';
import {timer} from 'rxjs';
import { Profile1Service } from './profile1.service';
import { AuthService } from './../servicios/auth.service';
import {TokenService} from './../servicios/token.service'
//import { LoginUsuario } from '../models/login-usuario';
import { FotoService } from './../servicios/foto.service';
import { Foto } from './../models/foto';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.page.html',
    styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit{

    //foto:FotoService[];  //guarda en un arreglo la lista de las fotos

    list: any;
    isLoading = true;
    isLogged=false;
    username=''; 
    perfilId: string;
  
    testLogged(): void {
        this.isLogged = this.tokenService.getToken() != null;
        this.username = this.tokenService.getUserName();
      }
    user = { 
        img:'', name: this.tokenService.getUserName()
    };
    
    constructor(private service: Profile1Service,public AuthService: AuthService, private tokenService: TokenService, 
        /*private FS:FotoService*/ private act:ActivatedRoute
        , private http: HttpClient) {
    }

    ngOnInit() {
        timer(2000).subscribe(r => {
           this.isLoading = !this.isLoading;
        }
        );
    }

    ionViewWillEnter(){
        this.testLogged();
    }


}