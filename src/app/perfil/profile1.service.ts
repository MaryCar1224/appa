import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class Profile1Service {
    getList(): Array<any> {
        return [
            {icon: 'podium', name: 'Estado de cuenta'},
            {icon: 'notifications-outline', name: 'Notificaciones'},
            {icon: 'card', name: 'Pagos'},
            {icon: 'basket', name: 'Compras'},
            {icon: 'heart', name: 'Favoritos'},
            {icon: 'person', name: 'Mis datos'},
            {icon: 'exit-outline', name: 'Salir'},
        ];
    }
}
