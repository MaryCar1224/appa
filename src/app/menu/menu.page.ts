import { ClienteService } from './../servicios/cliente.service';
import { ActivatedRoute } from '@angular/router';
import {Component, QueryList, ViewChildren} from '@angular/core';
import {IonRouterOutlet, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';
import { ToastProvider } from './../shared/providers/toast-provider';
import { AuthService } from './../servicios/auth.service';
import {NavController} from '@ionic/angular';
//import { ServiciosFoto } from './../models/servicios-foto';
//import { FotoService } from './../servicios/foto.service';
import { LoginUsuario } from '../models/login-usuario';
import { TokenService } from './../servicios/token.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage {
    lastBackPress = 0;
    timePeriodToExit = 2000;
    cliente:any;

    @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
    public pageList = [
        {
            iconName: 'home', displayText: 'Inicio', expanded: false, hasChild: false, url: '/menu/home'
        },
        {
            iconName: 'person-circle', displayText: 'Perfil', expanded: false, hasChild: false, url: '/menu/perfil/' + this.TokenService.getUserName()
        },
        {
            iconName: 'card', displayText: 'Pagos', expanded: false, hasChild: false, url: '/menu/pagos'
        },
        {
            iconName: 'today', displayText: 'Mis citas', expanded: false, hasChild: false, url: '/menu/citas'
        },
        {
            iconName: 'cart', displayText: 'Adquiere', expanded: false, hasChild: false, url: '/menu/adquiere'
        },
        {
            iconName: 'ticket', displayText: 'Compras', expanded: false, hasChild: false, url: '/menu/compras'
        },
        {
            iconName: 'newspaper', displayText: 'Noticias', expanded: false, hasChild: false, url: '/menu/noticias'
        },

        {
            iconName: 'easel', displayText: 'Clases', expanded: false, hasChild: true,
            subOptions: [
                {iconName: 'barbell', displayText: 'Cardio Dance', url: '/menu/cardio-dance'},
                {iconName: 'barbell', displayText: 'Gap', url: '/menu/cardio-dance'},
                {iconName: 'barbell', displayText: 'Step', url: '/menu/cardio-dance'},
                {iconName: 'barbell', displayText: 'Pilates', url: '/menu/cardio-dance'},
                {iconName: 'barbell', displayText: 'Yoga', url: '/menu/cardio-dance'},
                {iconName: 'barbell', displayText: 'Stretching', url: '/menu/cardio-dance'},
                {iconName: 'barbell', displayText: 'Body combat', url: '/menu/cardio-dance'},
                {iconName: 'barbell', displayText: 'Indoor ciclyng', url: '/menu/cardio-dance'},
                {iconName: 'barbell', displayText: 'Body fitness', url: '/menu/cardio-dance'},

            ]
        },
        {
            iconName: 'trophy', displayText: 'Retos', expanded: false, hasChild: true,
            subOptions: [
                {iconName: 'barbell', displayText: 'Reto 1', url: '/animation-list1'},
                {iconName: 'headset', displayText: 'Reto 2', url: '/animation-list2'},
                {iconName: 'infinite', displayText: 'Reto 3', url: '/animation-list3'},
                {iconName: 'leaf', displayText: 'Reto 4', url: '/animation-list4'},
                {iconName: 'medal', displayText: 'Reto 5', url: '/animation-list5'},
                {iconName: 'medical', displayText: 'Reto 6', url: '/animation-list6'},
                {iconName: 'nuclear', displayText: 'Reto 7', url: '/animation-list7'}
            ]
        },
        {
            iconName: 'exit', displayText: 'Salir', expanded: false, hasChild: false, url: '/menu/salir'
        }
    ];

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private toastProvider: ToastProvider,
        private screenOrientation: ScreenOrientation,
        private TokenService:TokenService,
        private ActivatedRoute:ActivatedRoute,
        private ClienteService:ClienteService
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            if (this.platform.is('cordova')) {
                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
            }
            this.backButton();
        });

        this.cargarCliente();
    }

    backButton() {
        this.platform.backButton.subscribeWithPriority(1, () => {
            this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
                if (outlet && outlet.canGoBack()) {
                    outlet.pop();
                } else {
                    if (new Date().getTime() - this.lastBackPress < this.timePeriodToExit) {
                        navigator['app'].exitApp();
                    } else {
                        this.toastProvider.show('Press back again to exit App');
                        this.lastBackPress = new Date().getTime();
                    }
                }
            });
        });
    }

    cargarCliente(){
        const id = this.ActivatedRoute.snapshot.params.id; //este es para obtener el id de la url
        //console.log(id);
        //toma una variable en este caso toma la del idcliente que se encuentra en la url
        this.ClienteService.obtCliente(id).subscribe(   //aqui no creo que sea necesario jalar el id
          (data)=>{
           console.log(data)  //muestra los datos en consola 
            this.cliente=data;
            //localStorage.getItem(this.cliente.club.nombre);
          },
          err =>{
          }
        )
    }
}
