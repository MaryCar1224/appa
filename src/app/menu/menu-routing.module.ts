import { ComprasPageModule } from './../pages/compras/compras.module';
import { HomePageModule } from './../pages/home/home.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children:[
      {
        path: 'comprade-servicios',
        loadChildren: () => import('../comprade-servicios/comprade-servicios.module').then( m => m.CompradeServiciosPageModule)
      },
      {
        path: 'perfil/:id',
        loadChildren: () => import('../perfil/perfil.module').then( m => m.PerfilPageModule),

      },
      {
        path: 'pagos',
        loadChildren: () => import('../pagos/pagos.module').then( m => m.PagosPageModule)
      },
      {
        path: 'anuncios',
        loadChildren: () => import('../anuncios/anuncios.module').then( m => m.AnunciosPageModule)
      },
      {
        path: 'formp',
        loadChildren: () => import('../formp/formp.module').then( m => m.FormpPageModule)
      },
      {
        path: 'home',
        loadChildren: () => import('../pages/home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'noticias',
        loadChildren: () => import('../pages/noticias/noticias.module').then( m => m.NoticiasPageModule)
      },
      {
        path: 'adquiere',
        loadChildren: () => import('../pages/adquiere/adquiere.module').then( m => m.AdquierePageModule)
      },
      {
        path: 'compras',
        loadChildren: () => import('../pages/compras/compras.module').then( m => m.ComprasPageModule)
      },
      {
        path: 'cardio-dance',
        loadChildren: () => import('../pages/entrenamientos/cardio-dance/cardio-dance.module').then( m => m.CardioDancePageModule)
      },
      {
        path: 'citas',
        loadChildren: () => import('../citas/citas.module').then( m => m.CitasPageModule)
      },
      
      {
        path:'login',
        loadChildren: () => import('../login/login.module').then( m => m.LoginPageModule)
      }

    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
