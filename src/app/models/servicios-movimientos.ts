export class ServiciosMovimientos {
    id?:number; //opcional
    idClienteMovimiento:number;
    idCliente:number;
    cliente:string;
    concepto:String;
    fechaDeAplicacion:Date;
    idOrdenDeVenta:number;
    idOrdenDeVentaDetalle:number;
    debito:number;
    saldo:number;
    activo:boolean;
    fechaCreacion:Date;
    fechaModificacion:Date;
 

}
