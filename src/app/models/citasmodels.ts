export class Citasmodels {
    id:number;
    nombre:string;
    tecnico:string;
    tipoActividad:string; 
    color:string;
    lugar:string;
    duracion:string;
    nivel:string;
    hora:string;
    cupo_actual:string;
    cupo_maximo:string;
    rango:string;
    disponible:boolean;
}
