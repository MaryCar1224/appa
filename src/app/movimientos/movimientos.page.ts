import { ActivatedRoute, Router } from '@angular/router';
import { ServiciosMovimientos } from './../models/servicios-movimientos';
import { MovimientosService } from './../servicios/movimientos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movimientos', 
  templateUrl: './movimientos.page.html',
  styleUrls: ['./movimientos.page.scss'],
})
export class MovimientosPage implements OnInit {

  movimientos:ServiciosMovimientos[] = [];
  mov:string;
  datos;
  total:number;

  constructor(
    private MovimientosService:MovimientosService,
    private ActivatedRoute:ActivatedRoute,
    ) { }

  ngOnInit() {
    this.cargar();
    this.SaldoTotal();
  }


  cargar(){
      const id = this.ActivatedRoute.snapshot.params.id;
      this.MovimientosService.movimientos(id).subscribe(
        data=>{
          console.log(data) 
          this.movimientos=data;
        }, 
        err =>{
         console.log(err);
        }

      )
      //this.mov = this.ActivatedRoute.snapshot.paramMap.get('id')
    //const id = this.ActivatedRoute.snapshot.params.id;
    //console.log(id);
    //toma una variable en este caso toma la del idcliente que se encuentra en la url
    //this.MovimientosService.movimientos(id).subscribe(
      //data=>{
        //this.movimientos=data;
      //},
      //err =>{
      //}
    //)
  }

  SaldoTotal(){
   //Calculamos el SALDO TOTAL 
   this.total = this.movimientos.reduce((
    acc,
    obj,
  ) => acc + (obj.saldo - obj.debito),
  0);
  console.log(" Saldo Total: ", this.total)
}


}
