import { FormsModule } from '@angular/forms';
import { MbscModule } from '@mobiscroll/angular';
import { interceptorProvider } from './interceptors/foto-interceptor.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule} from '@angular/common/http';
import {ComponentsModule} from './components/components.module';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],

  imports: [ 
    FormsModule,  
    MbscModule, BrowserModule, ComponentsModule,
    IonicModule.forRoot(), AppRoutingModule,
    HttpClientModule,ComponentsModule,BrowserAnimationsModule],

  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
  SplashScreen,StatusBar,ScreenOrientation,interceptorProvider],
  
  bootstrap: [AppComponent],
})
export class AppModule {}