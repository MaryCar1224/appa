import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClienteService } from './../servicios/cliente.service';

@Component({
  selector: 'app-misdatos',
  templateUrl: './misdatos.page.html',
  styleUrls: ['./misdatos.page.scss'],
})
export class MisdatosPage implements OnInit {

  cliente:any;

  constructor(private ActivatedRoute:ActivatedRoute, private ClienteService:ClienteService) { }

  ngOnInit() {
    this.cargarCliente();
  }

  cargarCliente(){
    const id = this.ActivatedRoute.snapshot.params.id; //este es para obtener el id de la url
    //console.log(id);
    //toma una variable en este caso toma la del idcliente que se encuentra en la url
    this.ClienteService.obtCliente(id).subscribe(   //aqui no creo que sea necesario jalar el id
      (data)=>{
       console.log(data)  //muestra los datos en consola 
        this.cliente=data;
        //localStorage.getItem(this.cliente.club.nombre);
      },
      err =>{
      }
    )
}

}
