import { HttpClient } from '@angular/common/http';
declare var require:any 
import { ToastController } from '@ionic/angular';
import { TokenService } from './../servicios/token.service';
import { AuthService } from './../servicios/auth.service';
import { Component, OnInit } from '@angular/core';
import { LoginUsuario } from '../models/login-usuario';
import { Router, ActivatedRoute } from '@angular/router';

var CryptoJS = require("crypto-js"); //importa la biblioteca necesaria para que pueda crifrar //se importa la libreria al proyecto
const secretKey = 'encriptadoclubalphaprueba'; //esta en la llave con la que se cifra la contraseña

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginUsuario: LoginUsuario;
  nombreUsuario = '';
  password = '';
  perfilId: string;

  isLogged = false; 
  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    private toastController:ToastController,
    private router: Router,
    private act: ActivatedRoute, 
    private http:HttpClient
    ){}
  
  ngOnInit() {
    this.testLogged();
  }

  ionViewWillEnter() {
    this.testLogged();
    this.vaciar();
  }
  onLogin() {
    var afterEncrypt = CryptoJS.DES.encrypt(this.password, CryptoJS.enc.Utf8.parse(secretKey), {  //método para cifrar la contraseña, solo le pasas el objeto (en this.password) y donde lo envies le pasas la variable a la que se le asigna el valor (afterEncrypt)
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }).toString()

    this.loginUsuario = new LoginUsuario(this.nombreUsuario, afterEncrypt); //aqui se envia el texto a el backend
    this.authService.login(this.loginUsuario).subscribe(
      data => {
        this.tokenService.setToken(data.token);
        this.tokenService.setUsername(data.nombreUsuario);
        this.tokenService.setAuthorities(data.authorities);
        this.isLogged = true;
        console.log("comprobando metodo");
        this.router.navigate(['/menu/home']); //nos lleva a la raíz "home", en este caso al inicio, modifique el enlace y redirige al perfil
 //'/menu/perfil/' + this.nombreUsuario
      },
    );
  }

  vaciar() {
    this.nombreUsuario = '';
    this.password = '';
  }

  async presentToast(msj: string) {
    const toast = await this.toastController.create({
      message: msj,
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }

  logOut(): void {
    this.tokenService.logOut();
    this.isLogged = false;
    this.vaciar();
  }

  testLogged(): void {
    this.isLogged = this.tokenService.getToken() != null;
  }
}