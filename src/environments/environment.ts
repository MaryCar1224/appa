// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  //ENDPOIN DEL APARTADO NOTICIAS
  directionNoticias: 'http://c42f-187-188-205-121.ngrok.io/clubalpha/api/v1/category/list/19/content',
  Token: `VGZAjMy3NliDlDNMTZjMzDm41ZFT7RYQMhj3DRN`,

  //ENDPOINT GLOBAL
  //global: 'http://localhost:8080/',  //LOCAL
  global: 'http://192.168.20.57:8090/' // URL SERVIDOR

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
